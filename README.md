
# Guess My Number Game

Welcome to the "Guess My Number" game!. This interactive game is designed with HTML, CSS, and JavaScript.

## How to Play

1. **Launch the Game:**
   - Open the HTML file (`index.html`) in a web browser.

2. **Game Interface:**
   - You will be greeted with the game interface featuring a title, input field, buttons, and score display.

3. **Make a Guess:**
   - Enter a number between 1 and 20 into the input field.
   - Click the "Check" button to submit your guess.

4. **Feedback:**
   - Receive feedback on your guess:
     - "Too high" or "Too low" if your guess is incorrect.
     - "Correct!" if your guess matches the hidden number.

5. **Scoring:**
   - Your current score is displayed, starting at 20.
   - The highscore represents the best score achieved during the session.

6. **Game Over:**
   - The game ends when you correctly guess the number or when your score reaches 0.

7. **Play Again:**
   - Click the "Again" button to reset the game and try again.

## Getting Started

1. **Clone the Repository:**
   ```bash
   git clone https://gitlab.com/thamvu107/guess-my-number.git
